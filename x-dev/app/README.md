
# Application README

## Overview

## Notes - Technical

- NOTE: See [ref](https://cloud.google.com/appengine/docs/standard/python3/testing-and-deploying-your-app#local-dev-server):
  "The updated dev_appserver does not support development of Python 3 apps on Windows".
  This means to run locally, the server must handle requests for all static content.
  - Run-time locally (non-GAE):
    - Watchers build Svelte JS and SCSS into /static
    - Python (starlette) serves /sc requests from /static
  - Build-time (before deploying to the cloud):
    - If needed, run builds for Svelte JS and SCSS into /static
    - Run build (gulp-remote-src) to save static HTML into /static
  - Run-time in the cloud (GAE):
    - GAE serves /sc requests from /static

### Server Responses

- API / JSON
  - Fabricated at run-time by Python code.
- HTML with data
  - Fabricated at run-time by Python code.
  - Example: /d?i=encrypted-data-string (legacy link from v1)
- HTML without data
  - For Local, fabricated at run-time by Python code.
    Why? The GAE SDK for Py3 on Windows doesn't
  - For Cloud, GAE serves as static files.   
- Javascript / Svelte
- Javascript / Other
- CSS / Svelte
- CSS / Other (scss)
- Images & Other
  - x 
  
