
# README - x-dev

DevOps stuff is placed here so as not to "pollute"
the application's root directory.

## NOTES - Application
- See [App README](app/README.md)

## NOTES - Python
- See [Python README](python/README.md)
- Has details for Python setup, adding packages, configuring IDEs.

## NOTES - NPM (DEVELOPER SET-UP)
- See [Node-NPM README](node-npm/README.md)
- Developer set-up (initial):
  - Install "node" (including "npm") if you don't already have it.
    See [NodeJS](https://nodejs.org/) website for instructions.
  - Install required NodeJS modules. These are not saved in git
    so they must be installed locally by each developer:
    - `cd c:\path\to\this-project\x-dev`
    - `npm install`
  - Add the useful `"scripts"` from `package.json` to your IDE's run
    configuration.  These include commands from running Hugo, Gulp, etc.
- Developer refresh (as required):
  - (TBD)
- NOTES:
  - When required, add additional npm packages:
    - `cd c:\path\to\this-project\x-dev`
    - `npm install --save-dev new-pkg`    (if needed for dev only)
    - `npm install --save-prod new-pkg`
  - When required, add additional useful commands to `package.json`
    so that they may be called in an IDE-independent way: `npm run my:cmd`.

## NOTES - Gulp
- See [Gulp README](gulp/README.md)

## NOTES - Google App Engine (GAE)
- See [GAE README](gae/README.md)

## NOTES - Misc
 - Material Icons (free, delivered with fonts)
   - &lt;i class="material-icons is-size-5">menu</i>
 - Fontawesome - OLD/DON'T
   - &lt;i class="fas fa-arrow-left"></i>
 - HTML / Unicode variables
   - Triangle pointers for use in buttons:
     - Solid   &#x25b2; &#x25b6; &#x25bc; &#x25c0;
     - Hollow: &#x25b3; &#x25b7; &#x25bd; &#x25c1;

