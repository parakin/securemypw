
# README - Python

## Initial Setup for Developers
- Initial set-up required for each developer's environment:
- Install git, Python 3.7, and pip.
- Get code from https://gitlab.com/parakin/securemypw.git
- Create Python virtual environment and install requirements:
- `cd c:\path\to\this-project`
- On Windows (using the [Python Launcher for Windows](https://docs.python.org/3/using/windows.html#python-launcher-for-windows)):
  - `py -3.7 -m venv venv`
  - `venv\Scripts\activate`
  - Note: the directory name must be "venv" (its used in package.json, .gitignore, .gcloudignore, etc)
- Install all dependencies at the previously frozen levels:
  - `pip install -r requirements.txt`

## Setup for IntelliJ IDE
- Click File > Project Structure > SDKs (under Platform Settings on left)
- In "Project Structure" modal, click "+" (in middle) > Python SDK.
- In "Add Python Interpreter" modal, click "Virtual Environment" (on left) 
  click "Existing Environment" radio button, then navigate to and
  select `.../venv/Scripts/python.exe`.
- Click ok, click ok.  SDK is now created and available.
- Click File > Project Structure > Project (under Project Settings on left)
- Under Project SDK, select from drop-down the SDK you just added.
- Click ok.  SDK is now selected for this project.

## Python Packages
- GAE expects to find "requirements.txt" in the project root directory with
  all dependencies listed (which should have version levels frozen/locked).
  This includes direct dependencies required by this app as well as the
  indirect dependencies required by the direct dependencies.
- When adding or upgrading direct dependencies:
  - Edit the file "x-dev\python\requirements.BASE.txt"
  - `pip install -r x-dev\python\requirements.BASE.txt`
  - `pip freeze > requirements.txt` 
