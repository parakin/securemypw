# ======================================================================
# requirements.BASE.txt
#   - The base/direct dependencies required by this project.
#   - Exclude all indirect dependencies (pip install will add them)
#   - After installing, `pip freeze` to capture installed levels.
# ======================================================================

# starlette:  web framework used by this app
starlette>=0.13.4
# uvicorn:  an ASGI server used to run starlette
uvicorn>=0.11.5
# gunicorn:  recommended for uvicorn prod environs
gunicorn>=20.0.4
# aiofiles:  optional dep of starlette for static files and file responses.
aiofiles>=0.5.0
# Jinja2:  optional dep of starlette for template responses (HTML, etc).
Jinja2==2.11.2
