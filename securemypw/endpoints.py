
from bnc4gae.starlette.jinja2 import Jinja2
from starlette.routing import Route
from starlette.routing import Mount
from starlette.staticfiles import StaticFiles
from starlette.endpoints import HTTPEndpoint
from starlette.responses import HTMLResponse, RedirectResponse

_jinja2 = Jinja2(directory='securemypw/templates')
_html_cache = {}


def get_routes():
    return [
        Route('/', RootPath, name='root'),
        Route('/en', HomePage, name='home'),
        Mount('/as', app=StaticFiles(directory='assets/static'), name="assets-static"),
        Mount('/ab', app=StaticFiles(directory='assets/built'), name="assets-built"),
    ]


class RootPath(HTTPEndpoint):
    async def get(self, request):
        return RedirectResponse(url='/en')


class HomePage(HTTPEndpoint):
    async def get(self, request):
        return _jinja2.get(request, 'ux1.base.html', cache=False)
