
from .endpoints import get_routes
from starlette.applications import Starlette
from starlette.responses import JSONResponse
from starlette.routing import Route
import uvicorn
import os


async def homepage(request):
    return JSONResponse({'hello': 'securemypw v2 world (gunicorn)'})


is_local = not os.getenv('GAE_ENV', '').startswith('standard')
app = Starlette(
    debug=is_local,
    routes=get_routes(),
)

# -------- Run Standalone
if __name__ == '__main__':
    print('Starting uvicorn ...')
    uvicorn.run('securemypw.main:app', port=8180, log_level='debug', reload=True)
