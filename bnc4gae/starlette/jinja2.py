
from starlette.requests import Request
from starlette.templating import Jinja2Templates

class Jinja2(Jinja2Templates):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._extra_data = {}
        self._cache = {}

    def add_extra_data(self, key: str, data):
        self._extra_data[key] = data

    def get(self, request: Request, template_name: str, data=None, cache=False):
        if cache and template_name in self._cache:
            return self._cache[template_name]
        data = data or {}
        data.update(self._extra_data)
        data['request'] = request
        response = self.TemplateResponse(template_name, data)
        if cache:
            self._cache[template_name] = response
        return response
